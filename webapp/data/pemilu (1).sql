-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 24, 2012 at 07:43 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pemilu`
--

-- --------------------------------------------------------

--
-- Table structure for table `kampanye`
--

CREATE TABLE IF NOT EXISTS `kampanye` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_calon` int(11) DEFAULT NULL,
  `profil` text,
  `visi` mediumtext,
  `misi` text,
  `program` text,
  `quotes` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userkampanye` (`id_calon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `komen`
--

CREATE TABLE IF NOT EXISTS `komen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pemilih` int(11) DEFAULT NULL,
  `subjek` varchar(200) DEFAULT NULL,
  `isi` text,
  `tampil` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userkomen` (`id_pemilih`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organisasi`
--

CREATE TABLE IF NOT EXISTS `organisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `konten` text,
  `sejarah` text,
  `logo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `suara`
--

CREATE TABLE IF NOT EXISTS `suara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_calon` int(11) DEFAULT NULL,
  `id_pemilih` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usercalon` (`id_calon`),
  KEY `userpemilih` (`id_pemilih`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `peran` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `email`, `peran`) VALUES
(1, 'riandy', 'riandy', 'riandy', 'riandyrn@gmail.com', 'calon'),
(2, 'sonny', 'sonny', 'sonny', 'sonnylazuardis@gmail.com', 'admin'),
(4, 'indra kusuma', 'indra', 'indrakusuma', 'indra@gmail.com', 'anggota');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kampanye`
--
ALTER TABLE `kampanye`
  ADD CONSTRAINT `userkampanye` FOREIGN KEY (`id_calon`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `komen`
--
ALTER TABLE `komen`
  ADD CONSTRAINT `userkomen` FOREIGN KEY (`id_pemilih`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suara`
--
ALTER TABLE `suara`
  ADD CONSTRAINT `usercalon` FOREIGN KEY (`id_calon`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `userpemilih` FOREIGN KEY (`id_pemilih`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
