<?php

/**
 * This is the model class for table "kampanye".
 *
 * The followings are the available columns in table 'kampanye':
 * @property integer $id
 * @property integer $id_calon
 * @property string $profil
 * @property string $visi
 * @property string $misi
 * @property string $program
 * @property string $quotes
 *
 * The followings are the available model relations:
 * @property User $idCalon
 */
class Kampanye extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Kampanye the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kampanye';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_calon', 'numerical', 'integerOnly'=>true),
			array('quotes', 'length', 'max'=>200),
			array('profil, visi, misi, program', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_calon, profil, visi, misi, program, quotes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCalon' => array(self::BELONGS_TO, 'User', 'id_calon'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_calon' => 'Id Calon',
			'profil' => 'Profil',
			'visi' => 'Visi',
			'misi' => 'Misi',
			'program' => 'Program',
			'quotes' => 'Quotes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_calon',$this->id_calon);
		$criteria->compare('profil',$this->profil,true);
		$criteria->compare('visi',$this->visi,true);
		$criteria->compare('misi',$this->misi,true);
		$criteria->compare('program',$this->program,true);
		$criteria->compare('quotes',$this->quotes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}