<?php

/**
 * This is the model class for table "komen".
 *
 * The followings are the available columns in table 'komen':
 * @property integer $id
 * @property integer $id_pemilih
 * @property string $subjek
 * @property string $isi
 * @property integer $tampil
 *
 * The followings are the available model relations:
 * @property User $idPemilih
 */
class Komen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Komen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'komen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pemilih, tampil', 'numerical', 'integerOnly'=>true),
			array('subjek', 'length', 'max'=>200),
			array('isi', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_pemilih, subjek, isi, tampil', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPemilih' => array(self::BELONGS_TO, 'User', 'id_pemilih'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_pemilih' => 'Id Pemilih',
			'subjek' => 'Subjek',
			'isi' => 'Isi',
			'tampil' => 'Tampil',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_pemilih',$this->id_pemilih);
		$criteria->compare('subjek',$this->subjek,true);
		$criteria->compare('isi',$this->isi,true);
		$criteria->compare('tampil',$this->tampil);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}