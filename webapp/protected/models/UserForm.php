<?php
/**
 * Register form model
 */
class UserForm extends CFormModel
{
    public $nama;
	public $username;
	public $password;
	public $email;
	public $verifyCode;
	
	public function rules()
	{
		return array(
			array('username', 'match', 'allowEmpty' => false, 'pattern' => '/[A-Za-z0-9\x80-\xFF]+$/'),
			array('username', 'length', 'max'=>200),
			array('email', 'email'),
			array('email, username', 'unique', 'className' => 'User' ),
			array('nama, username, email, password', 'required'),
			array('nama', 'length', 'max'=>200),
			array('password', 'length', 'min' => 6, 'max' => 32),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}
	
	/**
	 * Attribute values
	 *
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'nama' => 'Nama',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
		);
	}
	
}