<li class="dropdown">
	<a class="dropdown-toggle" href="#" data-toggle="dropdown">Masuk <strong class="caret"></strong></a>
	<div class="dropdown-menu" style="padding: 15px; padding-bottom: 0px;">
		<div class="form">
		<?php
			$model=new LoginForm;
		  $form=$this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'action'=>array('/site/login'),
			'htmlOptions'=>array('class'=>'form-vertical'),
			'enableClientValidation'=>true,
			//'enableAjaxValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>	
			<fieldset>
			<div class="control-group">
				<?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?>
				<section  class="controls">
				<?php echo $form->textField($model,'username',array('class'=>'controlku')); ?>
				<?php echo $form->error($model,'username',array('class'=>'help-inline')); ?>
				 </section>
			</div>

			<div class="control-group">
				<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
						<section  class="controls">
				<?php echo $form->passwordField($model,'password',array('class'=>'controlku')); ?>
				<?php echo $form->error($model,'password',array('class'=>'help-inline')); ?>
						</section>
			</div>
			<div class="help-block" >
				<?php echo $form->checkBox($model,'rememberMe',array('class'=>'controlku')); ?>
				<?php echo $form->label($model,"rememberMe",array('class'=>'checkbox help-inline')); ?>
				<?php echo $form->error($model,'rememberMe'); ?>
			</div>
			<div class="actions">
				<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary','style'=>'width:100%')); ?>
			</div>
			</fieldset>
			<?php $this->endWidget(); ?>
		</div>
	</div>
</li>