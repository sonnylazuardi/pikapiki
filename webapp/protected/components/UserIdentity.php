<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$users=User::model()->findByAttributes(array('username'=>$this->username));
		if($users===null){
		
			$this->errorCode=self::ERROR_USERNAME_INVALID;
			$this->errorMessage="identitas yang dimasukkan salah";
		}else if($users->password!=$this->password){
		
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
			$this->errorMessage="identitas yang dimasukkan salah";
		}else if($users->peran=='verifikasi'){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
			$this->errorMessage="Anda belum diverifikasi";
		}else {
			$this->_id = $users->id;
			$this->setState('peran', $users->peran);
			$this->setState('email', $users->email);
			$this->setState('nama', $users->nama);
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
	public function getId()
    {
        return $this->_id;
    }
}