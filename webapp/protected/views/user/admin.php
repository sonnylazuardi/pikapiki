<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
    <div class="page-header">
		<h1>Atur User <small>Atur semua user disini</small></h1>
	</div>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.BootGridView', array(
    'type'=>'striped',
    'dataProvider'=>$model->search(),
    'template'=>"{items}{pager}",
	'columns'=>array(
		'id',
		'nama',
		'username',
		'password',
		'email',
		'peran',
		array(
			'class'=>'CButtonColumn',
		),
    ),
)); ?>

