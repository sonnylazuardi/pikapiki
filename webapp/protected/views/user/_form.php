<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'nama',array('class'=>'control-label')); ?><section class="controls">
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nama',array('class'=>'help-inline')); ?></section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?><section class="controls">
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'username',array('class'=>'help-inline')); ?></section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?><section class="controls">
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'password',array('class'=>'help-inline')); ?></section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?><section class="controls">
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email',array('class'=>'help-inline')); ?></section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'peran',array('class'=>'control-label')); ?><section class="controls">
		<?php echo $form->dropDownList($model,'peran',array('admin'=>'admin','anggota'=>'anggota','calon'=>'calon')); ?>
		<?php echo $form->error($model,'peran',array('class'=>'help-inline')); ?></section>
	</div>

	<div class="form-actions buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Buat' : 'Simpan',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->