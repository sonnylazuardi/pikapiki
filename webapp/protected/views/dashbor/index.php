<div class="page-header">
<h1><?php echo Yii::app()->user->nama;?> <small>Dashbor</small></h1>
</div>

<?php
if(Yii::app()->user->peran=='admin')
	$this->renderPartial('adminIndex');
elseif(Yii::app()->user->peran=='calon')
	$this->renderPartial('calonIndex');
else
	$this->renderPartial('userIndex');

$this->renderPartial('calon',array(
			'dataProvider'=>$dataProvider,
));
?>