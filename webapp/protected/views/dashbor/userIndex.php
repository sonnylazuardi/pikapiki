<div class="row">
	<div class="span1 offset1">
		<?php $this->widget('ext.VGGravatarWidget.VGGravatarWidget', 
			array('email' => Yii::app()->user->email,'size'=>100,'htmlOptions'=>array('alt'=>Yii::app()->user->name)));
		?></div>
		<div class="span6">
			<p>
				Selamat datang di website pemilu, <?php echo Yii::app()->user->nama;?>. Pada panel ini Anda dapat mengubah 
				profil dan memilih calon. Sebelum memilih calon, Anda dapat melihat kampanye, visi, misi, dan profil para calon.
				Gunakan dengan bijak hak suara Anda.
			</p>
		</div>
</div>
