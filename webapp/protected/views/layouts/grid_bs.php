<?php $this->beginContent('//layouts/main'); ?>
    
	<nav id="main-nav" role="navigation">
        <!-- Bootstrap Navigation Bar Menu -->
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <!-- Be sure to leave the brand out there if you want it shown -->
                    <a class="brand" href="<?php echo Yii::app()->createUrl('/site/index');?>"></a>

                    <div class="nav-collapse">
						<?php $this->widget('zii.widgets.CMenu', array(
							  'items' => array(
								  array('label' => 'Home', 'url' => array('/site/index')),
								  array('label' => 'Tentang', 'url' => array('/site/about')),
								  array('label' => 'Contact', 'url' => array('/site/contact')),
								  array('label' => 'Dashbor', 'url' => array('/dashbor/'), 'visible' => !Yii::app()->user->isGuest),
								  array('label' => 'User', 'url' => array('/user/admin'), 'visible' => (Yii::app()->user->peran=='admin')),
								  //array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
								  //array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
							  ),
							  'htmlOptions' => array('class' => 'nav')
						 )); ?>
						<ul class="nav pull-right">
						  <?php if(Yii::app()->user->isGuest): ?>
							<li><a href="<?php echo Yii::app()->createUrl('/site/register');?>">Daftar</a></li>
						  <?php endif; ?>
						  <li class="divider-vertical"></li>
						  <?php if(Yii::app()->user->isGuest): ?>
									<?php //$this->widget('application.widgets.UserLogin'); ?>
									<?php CWidget::render('application.widgets.UserLogin');?>
						  <?php else: ?>
								<li><?php $this->widget('ext.VGGravatarWidget.VGGravatarWidget', 
									array('email' => Yii::app()->user->email,'size'=>60,'htmlOptions'=>array('alt'=>Yii::app()->user->name)));
								?></li>
								<ul class="nav">
								  <li class="dropdown">
									<a href="#" class="dropdown-toggle"
										  data-toggle="dropdown">										  
										  <?php echo Yii::app()->user->name; ?>
										  <b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo Yii::app()->createUrl('/user/updateme');?>">Profil</a></li>
										<li><a href="<?php echo Yii::app()->createUrl('/site/logout');?>">Logout</a></li>
									</ul>
								  </li>
								</ul>
								
						  <?php endif; ?>
						</ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
        <!-- Bootstrap Navigation Bar Menu -->
<div id="wrapper" class="container">
    <div class="row">
		<!--
        <div class='span12'>
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('bootstrap.widgets.BootBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
				)); ?>
			<?php endif?>
        </div>
		-->
		
        <div class='span9'>
            <?php
            $flashMessages = Yii::app()->user->getFlashes();
            if ($flashMessages) {
                echo '<div class="flash-messages">';
                foreach ($flashMessages as $key => $message) {
                    echo '<div class="alert alert-' . $key . '">' . "
          <a class='close' data-dismiss='alert'>×</a>
       {$message}
       </div>\n";
                }
                echo '</div>';
            }
            ?>
        </div>
        <div id="main" class="span9" role="main">

            <?php echo $content; ?>
        </div>
        <!-- main content -->
		
        <aside id="sidebar" class='span3'>
        	<?php echo $this->clips['sidebar']; ?>
        </aside>
        <!-- sidebar -->
    </div>
    <!-- row-->
    <footer class="footer">
        <div class="container">
            <div class="gbrfooter"></div>
            <p class='pull-right'>
                Copyright &copy; <?php echo date('Y'); ?> by PikaPiki team. Tugas Besar ARC<br/>
                All Rights Reserved. <?php echo Yii::powered(); ?>
            </div>
        </div>
    </footer>
</div>     <!-- wrapper -->
<?php $this->endContent(); ?>