<?php $this->pageTitle=Yii::app()->name; ?>
<div class="hero-unit">

<div class="row">
<div class="span2">
	<?php echo CHtml::image(Yii::app()->baseUrl.'/img/maskot.png');?>
</div>
<div class="span5">
	<h1>Buat Pemilu-mu Sekarang!</h1>
	<p>Lakukan pemilu yang aman, mudah dan hemat kertas</p>
	<h2></h2>
	<p></p>
</div>
</div>
<?php $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>'sidebar')); ?>
	<div class="well">
		<?php CWidget::render('application.widgets.DeskripsiSingkat');?>
	</div>
	<div class="well">
		<h2>Komen</h2>
		<p>
			Komen-komen terbaru disini
		</p>
		<?php echo CHtml::link('Kampanye',array('/site/page/about'),array('class'=>'btn '))?>
	</div>
	<div class="well">
		<h2>Statistik</h2>
		<p>
			Organisasi dalam PikaPiki
			Organisasi populer di PikaPiki
			Ketua di PikaPiki
		</p>
		<?php echo CHtml::link('Organisasi',array('/site/page/about'),array('class'=>'btn '))?>
		<?php echo CHtml::link('Pilih Calon',array('/site/page/about'),array('class'=>'btn '))?>	
	</div>
<?php $this->endWidget();?>			
<!--
<div class="row">
	<div class="span2">
	<?php echo CHtml::image(Yii::app()->baseUrl.'\img\diri.png'); ?>
	<div class="badge badge-warning">1</div>  <?php echo CHtml::link('Daftarkan Diri',array('/site/register'),array('class'=>'btn btn-primary btn-large'))?>
	</div>
	<div class="span2">
	<?php echo CHtml::image(Yii::app()->baseUrl.'\img\org.png'); ?>
	<div class="badge">2</div>  <?php echo CHtml::link('Organisasi',array('/org/create'),array('class'=>'btn btn-large'))?>
	</div>
	<div class="span2">
	<?php echo CHtml::image(Yii::app()->baseUrl.'\img\pensil.png'); ?>
	<div class="badge">3</div>  <?php echo CHtml::link('Pilih Calon',array('/user/vote'),array('class'=>'btn btn-large'))?>
	</div>
</div>
-->

</div>