<div class="page-header">
	<h1>Daftar <small>Daftar sebagai anggota</small></h1>
</div>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form-register-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('class'=>'form-horizontal'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?>
		<section class="controls">
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username',array('class'=>'help-inline')); ?>
		</section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'email',array('class'=>'control-label')); ?>
		<section class="controls">
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email',array('class'=>'help-inline')); ?>
		</section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'nama',array('class'=>'control-label')); ?>
		<section class="controls">
		<?php echo $form->textField($model,'nama'); ?>
		<?php echo $form->error($model,'nama',array('class'=>'help-inline')); ?>
		</section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
		<section class="controls">
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password',array('class'=>'help-inline')); ?>
		</section>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="control-group">
		<?php echo $form->labelEx($model,'verifyCode',array('class'=>'control-label')); ?>
		<section class="controls">
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Masukkan kata yang terlihat pada gambar diatas, kata tidak kasus-sensitif</div>
		<?php echo $form->error($model,'verifyCode',array('class'=>'help-inline')); ?>
		</section>
	</div>
	<?php endif; ?>


	<div class="form-actions buttons">
		<?php echo CHtml::submitButton('Daftar',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->