<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
    <div class="page-header">
		<h1>Masuk <small>Masuk ke dalam situs</small></h1>
	</div>
	
<div class="form">
<?php
  $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
   'htmlOptions'=>array('class'=>'form-vertical'),
	'enableClientValidation'=>true,
    //   'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); 

?>
    <fieldset>
	
	<div class="control-group">
		<?php echo $form->labelEx($model,'username',array('class'=>'control-label')); ?>
        <section  class="controls">
		<?php echo $form->textField($model,'username',array('class'=>'')); ?>
		<?php echo $form->error($model,'username',array('class'=>'help-inline')); ?>
         </section>
	</div>

	<div class="control-group">
		<?php echo $form->labelEx($model,'password',array('class'=>'control-label')); ?>
                <section  class="controls">
		<?php echo $form->passwordField($model,'password',array('class'=>'')); ?>
		<?php echo $form->error($model,'password',array('class'=>'help-inline')); ?>
                </section>
	</div>
	
	<div class="help-block" >
        <?php echo $form->checkBox($model,'rememberMe',array('class'=>'')); ?>
        <?php echo $form->label($model,"rememberMe",array('class'=>'checkbox help-inline')); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="form-actions">
		<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary')); ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>


</div><!-- form -->
