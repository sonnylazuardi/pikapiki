<?php
	if(!empty(Yii::app()->params['deskripsi'])){
		$p = new CHtmlPurifier();
		$p->options = array('URI.AllowedSchemes'=>array(
		  'http' => true,
		  'https' => true,
		));
		$text = $p->purify(Yii::app()->params['deskripsi']);
		echo CHtml::decode($text);
	}else{
		$this->renderPartial('pages/about');
	}

?>