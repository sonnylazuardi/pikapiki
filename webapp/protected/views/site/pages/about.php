<?php
$this->pageTitle=Yii::app()->name . ' - Tentang PikaPiki';
$this->breadcrumbs=array(
	'Tentang PikaPiki',
);
?>
<div class="page-header">
    <h1>PikaPiki? <small>Pilih Kanan Pilih Kiri</small></h1>
</div>
<div class="row">
	<div class="span3">
		<h3>Deskripsi</h3>
		<p>
			PikaPiki adalah kepanjangan dari Pilih Kanan Pilih Kiri yang merupakan sebuah aplikasi web 
			yang berguna untuk melakukan Pemilu (Pemilihan Umum). Kita bisa mendaftarkan diri untuk mengikuti 
			pemilihan untuk menjadi ketua atau pemimpin suatu badan organisasi. PikaPiki sangat sederhana dan mudah digunakan. 
		</p>
	</div>
	<div class="span3">
		<h3>Organisasi</h3>
		<p>
			PikaPiki mendukung berbagai jenis organisasi seperti kemahasiswaan, unit, dan lain sebagainya untuk membantu mengadakan 
			pemilihan umum di orgranisasi tersebut. Daftarkan segera organisasinya.
		</p>
	</div>
	<div class="span3">
		<h3>Calon dan Pemilih</h3>
		<p>
			Anggota PikaPiki yang sudah mendaftar akan menjadi pemilih sedangkan calon yang dipilih akan ditentukan oleh pihak 
			pengurus organisasi yang mendaftarkan organisasinya. Semua anggota berhak untuk memilih calonnya.
		</p>
	</div>
</div>
  
<div class="page-header">
    <h1>Core Team <small>teamwork makes the dream work</small></h1>
</div>
<div class="row">
	<div class="span3">
		<h3>Sonny Lazuardi</h3>
		<p>
			Seorang web developer biasa yang ingin mendalami dunia web.
		</p>
	</div>
	<div class="span3">
		<h3>Indra Kusuma</h3>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla condimentum venenatis. Duis eget ligula rhoncus risus 
			pellentesque eleifend. 
		</p>
	</div>
	<div class="span3">
		<h3>Riandy Rahman</h3>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla condimentum venenatis. Duis eget ligula rhoncus risus 
			pellentesque eleifend. 
		</p>
	</div>
	<div class="span3">
		<h3>Bagus Zukhri</h3>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fringilla condimentum venenatis. Duis eget ligula rhoncus risus 
			pellentesque eleifend.
		</p>
	</div>
</div>