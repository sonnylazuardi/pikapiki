<?php

class DashborController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/grid_bs';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin','generate'),
				'expression'=>'$user->peran=="admin"',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User', array(
			'criteria'=>array(
				'condition'=>'peran="calon"',
				//'order'=>'create_time DESC',
				//'with'=>array('author'),
			),
			'pagination'=>array(
				'pageSize'=>9,
			),
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	public function actionGenerate()
	{
		for($i=1;$i<=50;$i++){
			$model=new User;
			$model->nama = 'user'.$i;
			$model->username = 'user'.$i;
			$model->email = 'user'.$i.'@gmail.com';
			$model->password = 'user'.$i;
			$model->peran = 'anggota';
			$model->save();
		}
		echo 'sukses';
	}
}
